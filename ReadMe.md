# Koremu

A Password Generator.

- Good for telling others a password in impromptu situations.
- Good for remembering frequently used, but un-password-managable passwords.
- Good for making songs with wierd lyrics.

# Usage
This comes with node binary that allows you to use it like any other terminal utility.

```
$ npm install -g koremu
$ koremu
pigridypribripufra
```

if the node bin directory isn't in your path, you can also use `npx koremu`

You can also use it as a library. Run `npm install koremu` on you project.

```
// ES Modules: import koremu from 'koremu'
// CommonJS: const koremu = require('koremu')

console.log(koremu())

```

# Features
- Generates a password

# Upcoming in 1.0
- Pronounciation guide
- Minimum length option
- Pipeable output test


License: ISC
